<form method="post" action="/send-leave-post-request">
    <h2>Add your post here:</h2>
    <div class="field">
        <label for="post-name">Your name:</label>
        <input type="text" id="contact-name" placeholder="John Doe">
    </div>
    <div class="field">
        <label for="post-email">Your email:</label>
        <input type="email" id="contact-email" placeholder="email@example.com">
    </div>
    <div class="field">
        <label for="post-title">Title of your post:</label>
        <textarea id="contact-question" rows="4" placeholder="Please, enter your title"></textarea>
    </div>
    <div class="field">
        <label for="post-text">Text of your post:</label>
        <textarea id="contact-question" rows="4" placeholder="Please, enter your text"></textarea>
    </div>
    <button type="submit">Submit</button>
</form>
