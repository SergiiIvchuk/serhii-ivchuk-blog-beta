<div class="product-page">
    <img src="product-placeholder.jpeg" alt="<?= $data['title'] ?>" width="300"/>
    <h1><?= $data['title'] ?></h1>
    <h3><?= $data['author'] ?></h3>
    <p><?= $data['text'] ?></p>
    <p><span><?= $data['date'] ?></span></p>
    <a href="/contact-us">
        <button type="button">Comment</button>
    </a>
</div>

